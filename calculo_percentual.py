'''
    4) Dado o valor de faturamento mensal de uma distribuidora, detalhado por estado:
SP – R$67.836,43
RJ – R$36.678,66
MG – R$29.229,88
ES – R$27.165,48
Outros – R$19.849,53
Escreva um programa na linguagem que desejar onde calcule o percentual de representação 
que cada estado teve dentro do valor total mensal da distribuidora.

'''
faturamento_estado = {}
faturamento_estado['SP']=67836.43
faturamento_estado['RJ']=36678.66
faturamento_estado['MG']=29229.88
faturamento_estado['ES']=27165.48
faturamento_estado['Outros']=19849.53
faturamento_estado_bkp = faturamento_estado.copy();

print('---------------------------------------------')
print('Faturamento por estado: ', faturamento_estado)
print('---------------------------------------------')

total_faturado = 0
# somatório dos faturamentos
for i in faturamento_estado:
    #print(faturamento_estado[i])
    total_faturado += faturamento_estado[i]

porcentagem = 0
percentual_estado = {}

# cálculo da porcentagem e inclusão na lista percentual_estado
for i in faturamento_estado:
    porcentagem = (faturamento_estado[i] * 100) / total_faturado    
    percentual_estado[i]= porcentagem
    faturamento_estado_bkp[i] = format(porcentagem, ".2f")+'%'

print('---------------------------------------------')
print(f'Total Faturado: {total_faturado:.4f}')
print('---------------------------------------------')

'''
for i in percentual_estado:    
    print('Percentual Geral: ', format(percentual_estado[i],".2f"), '%')
'''

print('---------------------------------------------')
print('    Percentuais por estado    ')
print(faturamento_estado_bkp)
print('---------------------------------------------')