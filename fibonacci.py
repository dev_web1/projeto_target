'''
    2) Dado a sequência de Fibonacci, onde se inicia por 0 e 1 e o próximo valor sempre será a soma dos 2 valores anteriores (exemplo: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34...), escreva um programa na linguagem que desejar onde, informado um número, ele calcule a sequência de Fibonacci e retorne uma mensagem avisando se o número informado pertence ou não a sequência.

IMPORTANTE:
Esse número pode ser informado através de qualquer entrada de sua preferência ou pode ser previamente definido no código;

'''

from time import sleep

print ("\n" * 130)
print('--------------------------------------------------------------')
print('-       Programa Gerador da Sequência de Fibonacci           -')
print('--------------------------------------------------------------')

continuar = 'S'
while continuar != 'N':
    try:
        numero = int(input('\n**** Informe um número inteiro: '))
        contador = 1
        proximo  = 0
        anterior = 0
        atual    = 1

        sequencia_fibonacci = [0,1]

        while contador <= numero :
            contador = contador + 1
            proximo  = anterior + atual 
            anterior = atual
            atual    = proximo
            sequencia_fibonacci.append(proximo) #append -> insere o elemento na fila

        print('             ........................................')
        print('             |    Gerada Sequência de Fibonacci     |')
        print('             ........................................')

        print(sequencia_fibonacci)

                #count - contar o número de ocorrência de um elemento
        if (sequencia_fibonacci.count(numero)) > 0:
            print('\n--------------------------------------------------------------------')
            print('    Encontramos o número ', numero, ' na sequência de Fibonacci.   ')
            print('------------------------------------------------------------------------------------')
        else:
            print('\n---------------------------------------------------------------------')
            print('    O número ', numero, ' não se encontra na sequência de Fibonacci. ')
            print('---------------------------------------------------------------------')

    except:
        print('##### Você não digitou um número inteiro. ######\n\n')


    continuar = str(input('    >>>>>>>  Pressione tecla "N" para sair...  ou "ENTER" para continuar... '))
    if continuar.upper() == 'N':
        continuar ='N'
        print ("\n" * 130) 
        print('         ...................................')
        print('         :       Saindo do programa...     :')
        print('         ...................................')
        sleep(2)
        print ("\n" * 130) 
    elif continuar.upper() != 'N':
        continuar ='S'
        print ("\n" * 130) 
        print('             .......................................')
        print('             |     Gerar Sequência de Fibonacci    |')
        print('             .......................................')
