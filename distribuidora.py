'''
    3) Dado um vetor que guarda o valor de faturamento diário de uma distribuidora, faça um programa, na linguagem que desejar, que calcule e retorne:
• O menor valor de faturamento ocorrido em um dia do mês;
• O maior valor de faturamento ocorrido em um dia do mês;
• Número de dias no mês em que o valor de faturamento diário foi superior à média mensal.

IMPORTANTE:
a) Usar o json ou xml disponível como fonte dos dados do faturamento mensal;
b) Podem existir dias sem faturamento, como nos finais de semana e feriados. Estes dias devem ser ignorados no cálculo da média;

'''

import json
from pathlib import Path

#retornando o path do arquivo
path_dados = Path('dados.json').absolute()
print(path_dados)

# abrindo o arquivo json
with open(path_dados, encoding='utf-8') as meu_json:
    dados = json.load(meu_json)


fat_aux = 0
menor = 0
maior = 0
media = 0
soma  = 0
numero_de_dias = 0
dias_acima_media = []
valor_maior_media = []
dia_fat = {}
# para cada item do arquivo json
for i in dados:
    # imprimindo dia e valor formatados
    #print('Dia: ',i['dia'], 'R$ ', i['valor'])
    
    fat_aux = i['valor']
    # selecionando o maior e o menor faturamento
    if fat_aux > maior:
        maior = fat_aux
    else:
        menor = fat_aux

    #selecionando os dias com faturamento e calculando a media entre os dias com faturamento
    if fat_aux != 0:
        numero_de_dias += 1
        soma += fat_aux
        media = soma/numero_de_dias

# selecionando os dias com faturamento acima da média   
for i in dados:     
    if i['valor'] != 0:
        if media < i['valor']:
            dias_acima_media.append(i['dia'])
            valor_maior_media.append(i['valor'])
            dia_fat[i['dia']]= i['valor']

print('-------------------------------------------------')
print('Menor Faturamento: R$',menor)      
print('-------------------------------------------------')
print('Maior Faturamento: R$',maior)  
print('-------------------------------------------------')
print(f'Média: R$ {media:.4f}')
print('-------------------------------------------------')
print('Dias com faturamento: ', numero_de_dias)   
print('-------------------------------------------------')
print('Dias com faturamento acima da média: ', dias_acima_media) 
print('-------------------------------------------------')
print('Faturamento acima da média: R$ ', valor_maior_media) 
print('-------------------------------------------------')
print('Dias com seus respectivos faturamentos acima da média')
print(dia_fat)