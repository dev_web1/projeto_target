'''
    Programa que inverte um texto
'''


print('---------- Texto  ----------') 
texto = 'FRATERNIDADE PAZ E SAÚDE!'
print('        ',texto)

#lista
texto_inverso = []
#string
texto_inverso2 = ''

tam_txt = len(texto)
t=1
i=0
print('---------- Texto Reverso ----------') 
while t <= tam_txt:
    i = tam_txt-t
    texto_inverso += texto[i]
    texto_inverso2 += texto[i]
    t += 1
        
print('Texto Reverso como Lista: ', texto_inverso)
print('Texto Reverso como String: ', texto_inverso2)
